package com.walert.org.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author myworld1215
 */
@Component
public class RabbitmqReceiver {

    @RabbitListener(queues = "hello")
    public void process(String hello){
        System.out.println("Receiver: "+ hello);
    }
}
