package com.walert.org.repository;

import com.walert.org.entity.Visitor;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author myworld1215
 */
public interface  VisitorRepository extends JpaRepository<Visitor, Long> {
    Visitor findByIp(String ip);
}