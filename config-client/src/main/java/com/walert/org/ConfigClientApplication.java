package com.walert.org;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author myworld1215
 */
@SpringBootApplication
@RestController
@EnableDiscoveryClient
@RefreshScope
public class ConfigClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConfigClientApplication.class,args);
    }

    @Autowired
    private Environment environment;

    @Value("${user.name}")
    private String userName;

    @RequestMapping("/")
    public String home(){
        System.out.println(environment.getProperty("user.age"));
        System.out.println(environment.getProperty("user.sex"));
        System.out.println(environment.getProperty("user.name"));
        System.out.println(environment.getProperty("info.sys"));
        System.out.println(environment.getProperty("info"));

        return "may";
    }

    @GetMapping("/userName")
    public String getUserName(){
        return this.userName;
    }

}
