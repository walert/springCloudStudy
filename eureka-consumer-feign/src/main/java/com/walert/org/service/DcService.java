package com.walert.org.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author myworld1215
 */
@FeignClient("eureka-client")
public interface DcService {


    /**
     * dc接口
     * @return
     */
    @GetMapping("/dc")
    String consumer();

}
