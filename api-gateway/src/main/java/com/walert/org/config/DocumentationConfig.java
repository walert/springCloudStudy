package com.walert.org.config;

import org.springframework.cloud.netflix.zuul.filters.Route;
import org.springframework.cloud.netflix.zuul.filters.RouteLocator;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

@Component
@Primary
public class DocumentationConfig implements SwaggerResourcesProvider {

    private final RouteLocator routeLocator;

    public DocumentationConfig(RouteLocator routeLocator) {
        this.routeLocator = routeLocator;
    }

    @Override
    public List<SwaggerResource> get() {

        final List resources = new ArrayList();
//        resources.add(swaggerResource("Feign", "/eureka-consumer-feign/v2/api-docs", "2.0"));
//        resources.add(swaggerResource("service-b", "/swagger-service-b/v2/api-docs", "2.0"));

        List<Route> routes = routeLocator.getRoutes();
        routes.forEach(route->{
            System.out.println("routes："+route.getFullPath());
            resources.add(swaggerResource(route.getId(),route.getFullPath().replace("**","v2/api-docs"),"2.0"));
        });

        return resources;
    }

    private SwaggerResource swaggerResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}
