package com.walert.org;

import com.spring4all.swagger.EnableSwagger2Doc;
import com.walert.org.filter.MyFilter;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

/**
 * @author myworld1215
 */
@EnableSwagger2Doc
@EnableZuulProxy
@SpringCloudApplication
public class ApiGatewayApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(ApiGatewayApplication.class).web(true).run(args);
    }

//    @Bean
//    public MyFilter myFilter(){
//        return new MyFilter();
//    }


}
